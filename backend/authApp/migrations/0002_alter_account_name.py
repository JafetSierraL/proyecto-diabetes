# Generated by Django 3.2.7 on 2021-10-14 01:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authApp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='name',
            field=models.TextField(blank=True, null=True),
        ),
    ]
