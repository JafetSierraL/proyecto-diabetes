from django.db import models
from .account import Account

class Record(models.Model):
    recordId = models.AutoField(primary_key=True)
    age = models.IntegerField(default=False)
    pregnancies = models.IntegerField(default=False)
    glucose = models.FloatField(default=False)
    bloodpreassure = models.FloatField(default=False)
    insulin = models.FloatField(default=False)
    bmi = models.FloatField(default=False)
    tskinth = models.FloatField(default=False)
    record_account = models.ForeignKey(Account, related_name="my_account", on_delete=models.SET_NULL, null=True)
    
    