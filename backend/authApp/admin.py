from django.contrib import admin
from .models.account import Account
from .models.record import Record
from .models.result import Result
# Register your models here.
class AccountAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('id', 'name', 'email')
    search_fields = ['name']
    list_filter = ('name',)
 
class RecordAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('recordId','age','pregnancies','glucose','bloodpreassure','insulin','bmi','tskinth')

class ResultAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('resultId','date','result')

admin.site.register(Account, AccountAdmin)
admin.site.register(Record, RecordAdmin)
admin.site.register(Result, ResultAdmin)
