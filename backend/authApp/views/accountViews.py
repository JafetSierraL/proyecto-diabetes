from rest_framework import views, status
from rest_framework.response import Response
from rest_framework import generics
# Model
from authApp.models import Account
# Serializer
from authApp.serializers import AccountSerializer

class TestView(views.APIView):

    def get(self, request):
        response = {"message": "Esto es una prueba"}
        return Response(data=response, status=status.HTTP_200_OK)


class AccountView(views.APIView):

    def get(self, request):
        queryset = Account.objects.all()
        serializer = AccountSerializer(queryset, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        print("Información obtenida", request.data)
        serializer = AccountSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            data = {"message": "Se creó la cuenta"}
            return Response(data=data, status=status.HTTP_201_CREATED)
        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# List, Create
class AccountListCreateView(generics.ListCreateAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

# Read, Update, Delete
class AccountRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
