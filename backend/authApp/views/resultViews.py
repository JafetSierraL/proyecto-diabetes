from rest_framework import views, status
from rest_framework.response import Response
from rest_framework import generics

from authApp.models import Result

from authApp.serializers import ResultSerializer

class ResultView(views.APIView):
    
    def get(self,request):
        queryset = Result.objects.all()
        serializer = ResultSerializer(queryset, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)
    
    def post(self,request):
        print("Información obtenida", request.data)
        serializer = ResultSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            data = {"message": "Se añadio la informacion"}
            return Response(data=data, status=status.HTTP_201_CREATED)
        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
class ResultListCreateView(generics.ListCreateAPIView):
    queryset = Result.objects.all()
    serializer_class = ResultSerializer
    
class ResutlRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Result.objects.all()
    serializer_class = ResultSerializer