from rest_framework import fields, serializers
from authApp.models.result import Result


class ResultSerializer(serializers.ModelSerializer):
    class Meta:
       model =  Result
       fields = ['resultId','date','result']
       